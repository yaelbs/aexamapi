<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://yael-pro.firebaseio.com';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);